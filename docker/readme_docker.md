# TUTORIAL DOCKER

<br>

## Instalación

<br>

### Entornos y preparativos previos - APT y repositorio

<br>

1. Actualización de base de datos de paquetes

```
$ sudo apt-get update
```

2. Instalar dependencias y paquetes previos.

```
$ sudo apt-get install apt-transport-https ca-certificates curl
gnupg2 software-properties-common
```

3. la llave GPG oficial

```
$ sudo curl -fsSL https://download.docker.com/linux/debian/gpg |
sudo apt-key add -
```

4. Para reconocer el `add-apt-repository`

```
sudo apt-get install software-properties-common
```

5. Añadimos el repositorio ‘stable’

```
$ sudo add-apt-repository "deb [arch=amd64]
https://download.docker.com/linux/ubuntu $(lsb_release -cs)
stable"
```

6. Actualizar base de datos de paquetes

```
$ sudo apt-get update
```

<br>

### Docker Engine Community - Instalación

<br>

1. Instalar docker engine

```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

2. Verificar Instalación

```
$ sudo docker run hello-world
```

<br>

## Ejecución del docker

**Crear Imágenes a partir del Dockerfile**

```
$ docker build -t abmimage . 
```

**Correr el correr un contenedor de servidor inverso:**

```
$ docker run -d -p 80:80 \
-v /var/run/docker.sock:/tmp/docker.sock:ro \
jwilder/nginx-proxy
```

**Correr el contedor a partir de nuestra imagen**

```
$ docker run -d --name abmcontenedor \
-e APPSERVERNAME=abm.istea \
-e APPALIAS=www.abm.istea \
-e MYSQL_ROOT_PASSWORD=bacaza123 \
-e MYSQL_USER=abmdbuser \
-e MYSQL_USER_PASSWORD=bagaza123 \
-e MYSQL_DB_NAME=eduvin \
-v /tmp/abm/www:/var/www/html \
-e VIRTUAL_HOST=abm.istea \
abmimage
```

**Opciones:**
<br>`-d`: lo deja en segundo plano
<br>`--name`: para elegir el nombre del contenedor (registrocontendedor)
<br>`-e`: para pasar las variables de entorno
<br>`-v`: bind de montaje de un volumen
<br>`-p`: mapeamento de puertos (<host port>:<contenedor port>)


Para acceder al sitio hay que agregar el host `registro.istea`. En Windows lo encontrás en `C:\Windows\System32\drivers\etc\host`, y en linux debia está en `/etc/hosts`.

Luego, desde un navegador tipeás `http://registro.istea`.

<br>

## Subiendo una imagen al DockerHub

Lo prinmero es crear una imagen docker a partir de un contenedor. Está imagen puede contener modificaciones hechas despúes de que corremos el contenedor. 

```
$ docker commit abmcontenedor viniciusistea/abm:1 
```
`abmcontenedor:` nombre del contenedor que usaremos como base
`viniciusistea/abm:` [nombre de la cuenta]/[nombre del repositorio]:[tag]
En nuestro caso la `tag` será el número de versionado.

<br>

Podemos ver la imagen creada con el comando abajo

```
$ docker images
```

<br>

Logueamos a nuestra cuenta de DockerHub, seguido de la password.

```
$ docker login
```

<br>

Subimos un nuevo repositorio chamado abm:

```
$ docker push viniciusistea/abm:1
```

`Obs: Para pushear una nueva tag a este repositorio:`

```
$ docker push viniciusistea/abm:tagname
```

<br>

Podemos bajar la imagen con el comando abajo:
```
$ docker pull viniciusistea/abm:1
```

O simplemente correr el contenedor directamente, mencionándola:

```
$ docker run -d --name abmcontenedor \
-e APPSERVERNAME=abm.istea \
-e APPALIAS=www.abm.istea \
-e MYSQL_ROOT_PASSWORD=bacaza123 \
-e MYSQL_USER=abmdbuser \
-e MYSQL_USER_PASSWORD=bagaza123 \
-e MYSQL_DB_NAME=eduvin \
-v /tmp/abm/www:/var/www/html \
-e VIRTUAL_HOST=abm.istea \
viniciusistea/abm:1
```







## Comandos Importantes en Docker

<br>

Ver procesos/contenedores y sus estados:

```
$ docker ps -a
```

<br>

Listar contenedores por sus IDs:

```
$ docker ps -a -q
```

<br>

Eliminar contenedor:

```
$ docker rm <contenedor>
```

<br>

Eliminar todos los contenedores:

```
$ docker rm $(docker ps -a -q)
```

`$(docker ps -a -q)`: devuelve todos los IDs

<br>

Controlar contenedores:

```
$ docker start <container name>
$ docker stop <container name>
```

<br>

Listar imagenes: 

```
$ docker images
```

<br>

Borrar Imagenes:

```
$ docker rmi <image names>
```

<br>

Borrar todas imagenes:

a ) Parar todas los containers:

```
$ docker stop $(docker ps -a -q)
```
<br>

b ) Borrar todas las imagenes:

```
$ docker rmi $(docker images -q)
```

<br>

Executar comandos dentro de un contenedor:

```
$ docker exec -ti dockercontenedor /bin/bash 

```

<br>





