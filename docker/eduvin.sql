-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-07-2019 a las 18:00:03
-- Versión del servidor: 10.3.15-MariaDB
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `eduvin`
--
CREATE DATABASE IF NOT EXISTS `eduvin` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `eduvin`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL COMMENT 'ID Categorias',
  `nombrecategoria` varchar(128) NOT NULL COMMENT 'Nombre Categoria'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombrecategoria`) VALUES
(5, 'Televisores'),
(6, 'Electrodomesticos'),
(12, 'Lavarropas y Secarropas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moneda`
--

CREATE TABLE `moneda` (
  `idmoneda` int(11) NOT NULL COMMENT 'ID Moneda',
  `nombremoneda` varchar(128) NOT NULL COMMENT 'Nombre Moneda',
  `simbolomoneda` varchar(5) NOT NULL COMMENT 'Simbolo Moneda'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `moneda`
--

INSERT INTO `moneda` (`idmoneda`, `nombremoneda`, `simbolomoneda`) VALUES
(1, 'Pesos', '$'),
(4, 'Dolares', 'U$S'),
(7, 'Reales', 'R$'),
(8, 'Euros', 'E');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio`
--

CREATE TABLE `precio` (
  `id` int(11) NOT NULL COMMENT 'ID Precio',
  `numeroprecio` int(10) NOT NULL COMMENT 'Precio',
  `idmoneda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `precio`
--

INSERT INTO `precio` (`id`, `numeroprecio`, `idmoneda`) VALUES
(2, 5000, 4),
(23, 1000, 1),
(24, 600, 1),
(34, 800, 4),
(35, 250, 7),
(36, 900, 8),
(37, 220, 8),
(41, 2000, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL COMMENT 'ID Producto',
  `nombreproducto` varchar(128) NOT NULL COMMENT 'Nombre Producto',
  `descripcionproducto` varchar(128) NOT NULL COMMENT 'Descripcion Producto',
  `idsubcategoria` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `idprecio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombreproducto`, `descripcionproducto`, `idsubcategoria`, `idproveedor`, `idprecio`) VALUES
(1, 'Smart TV', 'Televisor Smart TV', 3, 5, 2),
(15, 'Lavarropas Dream', 'Lavarropas automatico de carga superior', 4, 11, 34),
(24, 'Cafetera Dream', 'Cafetera de 1.5 litros programable', 21, 1, 23),
(25, 'Batidora de mano', 'Batidora de mano Dream con accesorios', 22, 1, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL COMMENT 'ID Proveedor',
  `nombre` varchar(128) NOT NULL COMMENT 'Nombre Proveedor',
  `direccion` varchar(128) NOT NULL COMMENT 'Direccion Proveedor',
  `cuit` varchar(11) NOT NULL COMMENT 'CUIT Proveedor',
  `telefono` varchar(15) NOT NULL COMMENT 'Telefono Proveedor',
  `idtipoproveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `nombre`, `direccion`, `cuit`, `telefono`, `idtipoproveedor`) VALUES
(1, 'Garbarino', 'Lavalle 648', '30500053522', '41312100', 3),
(5, 'Fravega', 'Peron 440', '30526874249', '08103338700', 3),
(11, 'Eduardo Diaz', 'Av.Presidente Peron 440', '20260435540', '42833275', 1),
(12, 'La Casa del Audio', 'Hipolito Yrigoyen 8862', '30707467963', '42458668', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `id` int(11) NOT NULL COMMENT 'ID SubCategorias',
  `nombresubcategoria` varchar(128) NOT NULL COMMENT 'Nombre Subcategoria',
  `idcategorias` int(11) NOT NULL COMMENT 'ID Categorias'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `nombresubcategoria`, `idcategorias`) VALUES
(3, 'Televisores Smart', 5),
(4, 'Lavarropas y Secarropas', 6),
(21, 'Cafeteras', 6),
(22, 'Batidoras y Licuadoras', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodeproveedor`
--

CREATE TABLE `tipodeproveedor` (
  `id` int(11) NOT NULL COMMENT 'ID Tipo de proveedor',
  `condicion` varchar(128) NOT NULL COMMENT 'Nombre condicion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla de tipo de proveedores';

--
-- Volcado de datos para la tabla `tipodeproveedor`
--

INSERT INTO `tipodeproveedor` (`id`, `condicion`) VALUES
(1, 'Consumidor Final'),
(2, 'Excento'),
(3, 'Responsable Inscripto'),
(5, 'Monotributo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`idmoneda`);

--
-- Indices de la tabla `precio`
--
ALTER TABLE `precio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idmoneda` (`idmoneda`) USING BTREE;

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idprecio` (`idprecio`) USING BTREE,
  ADD KEY `idproveedor` (`idproveedor`) USING BTREE,
  ADD KEY `idsubcategoria` (`idsubcategoria`) USING BTREE;

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idproveedor` (`idtipoproveedor`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcategorias` (`idcategorias`) USING BTREE;

--
-- Indices de la tabla `tipodeproveedor`
--
ALTER TABLE `tipodeproveedor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Categorias', AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `moneda`
--
ALTER TABLE `moneda`
  MODIFY `idmoneda` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Moneda', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `precio`
--
ALTER TABLE `precio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Precio', AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Producto', AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Proveedor', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID SubCategorias', AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `tipodeproveedor`
--
ALTER TABLE `tipodeproveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID Tipo de proveedor', AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `precio`
--
ALTER TABLE `precio`
  ADD CONSTRAINT `precio_ibfk_1` FOREIGN KEY (`idmoneda`) REFERENCES `moneda` (`idmoneda`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`idsubcategoria`) REFERENCES `subcategorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`idproveedor`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `producto_ibfk_3` FOREIGN KEY (`idprecio`) REFERENCES `precio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`idtipoproveedor`) REFERENCES `tipodeproveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD CONSTRAINT `subcategorias_ibfk_2` FOREIGN KEY (`idcategorias`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
